<?php
echo "hello php neo4j 1</br>";
require_once 'vendor/autoload.php';
 
use Neoxygen\NeoClient\ClientBuilder;

// $connUrl = parse_url('http://master.sb02.stations.graphenedb.com:24789/db/data/');
// $user = 'neo4j';
// $pwd = 'asdasdasd';
// $client = ClientBuilder::create()
//   ->addConnection('default', $connUrl['scheme'], $connUrl['host'], $connUrl['port'], true, $user, $password)
//   ->build();
$client = ClientBuilder::create()
    ->addConnection('default','http','127.0.0.1',7474,true,'neo4j','asdasdasd')
    ->setAutoFormatResponse(true)
    ->setDefaultTimeout(200)
    ->build();
$version = $client->getNeo4jVersion();
echo $version;
$query = 'CREATE (user:User {name:"李滨"}) RETURN user';
$result = $client->sendCypherQuery($query)->getResult();
 
$user = $result->getSingleNode();
$name = $user->getProperty('name');
echo $name;
$query = 'MATCH (user1:User {name:"李滨"}), (user2:User {name:"万义鹏"}) CREATE (user1)-[:FOLLOWS]->(user2)';
$params = ['user1' => '李滨', 'user2' => '万义鹏'];
#$client->sendCypherQuery($query, $params);
$client->sendCypherQuery($query);
$result = $client->run('match(n) delete n');
// $result = $client->run('MATCH (m:person{name:"李滨"})-[r]-(n) RETURN m,r,n');
// foreach ($result->getRecords() as $r) {
//     //获取节点标签
//     print_r($r->get('n')->labels());
//     //获取节点所有属性的值
//     print_r($r->get('n')->values());
//     //获取节点所有属性的key
//     print_r($r->get('n')->keys());
//     //获取节点某个属性的值,不存在则返回空
//     print_r($r->get('n')->value("uid"));
//     //获取关系类型
//     print_r($r->get('r')->type());
//     //获取关系属性
//     print_r($r->get('r')->value('time'));exit();
// }
?>